(function($) {
  Drupal.behaviors.recurly_checkall = {
      attach: function(context, settings) {

  	    $('.checkall').click(function() {
  		  $(this).parents('fieldset:eq(0)').find(':checkbox').attr('checked', this.checked);
  		});

      }
    };
})(jQuery);