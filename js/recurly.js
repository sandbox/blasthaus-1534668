(function($) {
  Drupal.behaviors.recurly = {
      attach: function(context, settings) {

        if ($('div.dataTables_wrapper').length == 0) {
    	     oTable = $('#recurly-accounts').dataTable({
               "bJQueryUI": true,
                "bPaginate": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 25
    		});
          }

      }
    };
})(jQuery);