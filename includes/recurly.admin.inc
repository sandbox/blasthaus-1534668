<?php
/**
 * @file recurly.admin.inc
 * Recurly settings forms and administration page callbacks.
 */

define('RECURLY_V2_ENDPOINT', 'https://api.recurly.com/v2/');
define('RECURLY_ACCOUNT_PERPAGE', '200');

/**
 * Returns the site-wide Recurly settings form.
 */
function recurly_settings_form($form, &$form_state) {
  // Add form elements to collect default account information.
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default account settings'),
    '#description' => t('Configure these settings based on your Company Settings and API Credentials settings in the Recurly administration interface.'),
    '#collapsible' => TRUE,
  );
  $form['account']['recurly_subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#description' => t("The subdomain of your account including the -test suffix if using the Sandbox."),
    '#default_value' => variable_get('recurly_subdomain', ''),
  );
  if (module_exists('recurly_hosted')) {
    $form['account']['recurly_hosted_payment_pages'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hosted Payment Pages are enabled for this account.'),
      '#default_value' => variable_get('recurly_hosted_payment_pages', FALSE),
    );
  }
  $form['account']['recurly_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('recurly_api_key', ''),
  );
  $form['account']['recurly_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Recurly Private Key'),
    '#description' => t('Optional: Recurly Private Key - enter this if needed for transparent post/recurly.js verifications.'),
    '#default_value' => variable_get('recurly_private_key', ''),
  );

  // Add form elements to configure default push notification settings.
  $form['push'] = array(
    '#type' => 'fieldset',
    '#title' => t('Push notification settings'),
    '#description' => t('If you have supplied an HTTP authentication username and password in your Push Notifications settings at Recurly, your web server must be configured to validate these credentials at your listener URL.'),
    '#collapsible' => TRUE,
  );
  $form['push']['recurly_listener_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Listener URL key'),
    '#description' => t('Customizing the listener URL gives you protection against fraudulent push notifications.') . '<br />' . t('Based on your current key, you should set @url as your Push Notification URL at Recurly.', array('@url' => url('recurly/listener/' . variable_get('recurly_listener_key', ''), array('absolute' => TRUE)))),
    '#default_value' => variable_get('recurly_listener_key', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#field_prefix' => url('recurly/listener/', array('absolute' => TRUE)),
  );
  $form['push']['recurly_push_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log authenticated incoming push notifications. (Primarily used for debugging purposes.)'),
    '#default_value' => variable_get('recurly_push_logging', FALSE),
  );
  
  // Add form elements allowing the administrator to toggle integration options.
  $form['integration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Integration options'),
    '#collapsible' => TRUE,
  );
  $profile_display = (module_exists('recurly_hosted')) ? array('profile_display' => t('Display Recurly account information on user profile pages, including account management links if Hosted Payment Pages are enabled.')) : array();
  
  $form['integration']['recurly_account_integration'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Account integration'),
    '#options' => array_merge($profile_display, array(
      'push_create' => t('Create local account records upon receipt of push notifications linked to users by e-mail address. For maximum security, this should only be enabled if e-mail account verification is required during registration.'),
      'push_update' => t('Update local account records upon receipt of push notifications. This option can be used without creating local account records on notifications, but if you are, you should most likely be updating them, too.'))
    ),
    '#default_value' => variable_get('recurly_account_integration', array()),
  );
  
  return system_settings_form($form);
}

/**
 * Trims user-supplied API text values.
 */
function recurly_settings_form_validate($form, &$form_state) {
  static $keys = array(
    'recurly_subdomain',
    'recurly_api_username',
    'recurly_api_password',
    'recurly_listener_key',
  );
  foreach ($keys as $key) {
    $form_state['values'][$key] = trim($form_state['values'][$key]);
  }
}

/**
 * Returns the base Recurly URL for the current account with an optional path
 * appended to it.
 */
function recurly_url($path = '') {
  // Generate the subdomain to use for the current account.
  $subdomain = variable_get('recurly_subdomain', '');

  return url('https://' . $subdomain . '.recurly.com/' . $path);
}

/**
 * Returns an edit URL for a subscription plan.
 *
 * @param $plan
 *   The subscription plan object returned by the Recurly client.
 *
 * @return
 *   The URL for the plan's edit page at Recurly.
 */
function recurly_subscription_plan_edit_url($plan) {
  return recurly_url('company/plans/' . $plan->plan_code);
}

/**
 * Displays a list of subscription plans currently defined in your Recurly account.
 */
function recurly_subscription_plans_overview() {
  // Initialize the Recurly client with the site-wide settings.
  if (!recurly_client_initialize()) {
    return t('Could not initialize the Recurly client.');
  }

  // Retrieve the plans for the current account.
  $plans = recurly_subscription_plans();

  // Format the plan data into a table for display.
  $header = array(t('Subscription plan'), t('Price'), t('Setup fee'), t('Trial'), t('Created'), t('Operations'));
       
  $rows = array();
  foreach ($plans as $plan) {
      
    $operations = array();
    $description = '';

    // Prepare the description string if one is given for the plan.
    if (!empty($plan->description)) {
      $description = '<div class="description">' . check_plain($plan->description) . '</div>';
    }

    // Add an edit link if available for the current user.
    if (user_access('edit subscription plans')) {
      $operations[] = array(
        'title' => t('edit'),
        'href' => recurly_subscription_plan_edit_url($plan),
      );
    }

    // Add a purchase link if Hosted Payment Pages are enabled.
    if (module_exists('recurly_hosted') && variable_get('recurly_hosted_payment_pages', FALSE)) {
      $operations[] = array(
        'title' => t('purchase'),
        'href' => recurly_hosted_subscription_plan_purchase_url($plan),
      );
    }

    $unitamount = str_replace(array('<', '[', ']', '>', ' ', 'Recurly_CurrencyList'), '', $plan->unit_amount_in_cents->__toString());
    $setupfee = str_replace(array('<', '[', ']', '>', ' ', 'Recurly_CurrencyList'), '', $plan->setup_fee_in_cents->__toString());
    $unitamount = str_replace(array('USD=', 'EUR='), array('$', '€'), $unitamount);
    $setupfee = str_replace(array('USD=', 'EUR='), array('$', '€'), $setupfee);
        
    $rows[] = array(
      t('<h6 style="padding:0;margin:0">@name</h6><div class="sku">(Plan code: @code)</div>', array('@name' => $plan->name, '@code' => $plan->plan_code)) . $description,
      t('<h6 style="padding:0;margin:0">@unit_price</h6> <span style="font-size: 9px;">per @interval_length @interval_unit</span>', array('@unit_price' => $unitamount, '@interval_length' => $plan->plan_interval_length, '@interval_unit' => $plan->plan_interval_unit)),
      t('@setup_fee', array('@setup_fee' => $setupfee)),
      t('@trial_length @trial_unit', array('@trial_length' => $plan->trial_interval_length, '@trial_unit' => $plan->trial_interval_unit)),
      t('<small>@date</small>', array('@date' => recurly_format_date($plan->created_at))),
      theme('links', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline')))),
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No subscription plans found.'), 'colspan' => 6));
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/* Returns a formatted price for display purposes */
function recurly_format_price($price_in_cents, $currency) {
  if (module_exists('commerce')) {
    return commerce_currency_format($price_in_cents, $currency, NULL, TRUE);  
  }
  return ($currency == 'USD' || $currency == 'EU') ? str_replace(array('USD', 'EUR'), array('$', '€'), $currency) . number_format($price_in_cents/100, 2, '.', ',') : number_format($price_in_cents/100, 2, '.', ',') . ' ' . $currency;

}

/* Returns a formatted date for display purposes */
function recurly_format_date($date) {
  if (is_array($date)) {
    return '';
  }
  return (is_object($date)) ? $date->format('M-d-Y h:s') : format_date(strtotime($date), 'custom', 'M-d-Y h:s'); 
}

/* Adds js and css files when viewing a ctools modal table */
function recurly_add_js($width=1000, $height=550) {
  // load js and css for the table ui 
  if (module_exists('libraries') && module_exists('ctools') && ($path = libraries_get_path('datatables')) && file_exists($path . '/media/js/jquery.dataTables.min.js')) {

    // Include css files to style table, etc
    drupal_add_css(drupal_get_path('module', 'recurly') . '/css/recurly.css');
    drupal_add_library('recurly', 'datatables');
    // Include the CTools tools that we need.
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    $licenses_style = array(
      'ctools-licenses-style' => array(
        'modalSize' => array(
        'type' => 'fixed',
        'width' => $width,
        'height' => $height,
        'addWidth' => 20,
        'addHeight' => 15,
        ),
        'modalOptions' => array(
          'opacity' => .5,
          'background-color' => '#000',
        ),
        'animation' => 'fadeIn',
        'modalTheme' => 'CToolsCustomModal',
        'throbber' => theme('image', array('path' => ctools_image_path('ajax-loader.gif', 'ctools_ajax_sample'), 'alt' => t('Loading...'), 'title' => t('Loading'))),
        ),
    );

    drupal_add_css(drupal_get_path('module', 'recurly') . '/css/recurly-ctools-ajax.css');
    drupal_add_js(drupal_get_path('module', 'recurly') . '/js/recurly-ctools-ajax.js');     
    drupal_add_js(drupal_get_path('module', 'recurly') . '/js/recurly.js');
    drupal_add_js($licenses_style, 'setting');
  }
}

/* callback returns subscription data from Recurly in a table view for an account code */
function recurly_subscription_view($account_code=FALSE) {
    
  if ($account_code) {
    $output = '';
  // Initialize the Recurly client with the site-wide settings.
    if (!recurly_client_initialize()) {
      return t('Could not initialize the Recurly client.');
    }
              
    // get the subscriptions for this account code at Recurly
    $subs = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts/' . $account_code . '/subscriptions', '', TRUE);

    // Format the subscription data into a table for display.
    $header = array(t('Subscription plan'), t('Price'), t('State'), t('Setup fee'), t('Trial'), t('Current'), t('Created'));

    $rows = array();

    foreach ($subs as $sub) {
      $rows[] = array(
        t('<h6 style="padding:0;margin:0">@name</h6><div class="sku">(Plan code: @code)</div>', array('@name' => $sub['plan']['name'], '@code' => $sub['plan']['plan_code'])),
        t('<h6 style="padding:0;margin:0">@unit_price</h6>', array('@unit_price' => recurly_format_price($sub['unit_amount_in_cents'], $sub['currency']))),
        t('@state', array('@state' => $sub['state'])),
        t('@setup_fee', array('@setup_fee' => (isset($sub['setup_fee'])) ? recurly_format_price($sub['setup_fee'], $sub['currency']) : '')),
        t('@trial_start - @trial_ends', array('@trial_start' => (isset($sub['trial_started_at']) && is_string($sub['trial_started_at'])) ? recurly_format_date($sub['trial_started_at']) : '', '@trial_ends' => (isset($sub['trial_ends_at']) && is_string($sub['trial_ends_at'])) ? recurly_format_date($sub['trial_ends_at']) : '')),
        t('@current_start - @current_end', array('@current_start' => recurly_format_date($sub['current_period_started_at']), '@current_end' => recurly_format_date($sub['current_period_ends_at']))),
        t('@date', array('@date' => recurly_format_date($sub['activated_at']))),
        );

      if (!empty($sub['pending_subscription'])) {
        $rows[] = array(
          t('<h6 style="padding:0;margin:0;color:#FF0000">@name</h6><div class="sku">(Plan code: @code)</div><div class="sku">(Change from plan code: @prevcode)</div>', array('@name' => $sub['pending_subscription']['name'], '@code' => $sub['pending_subscription']['plan_code'], '@prevcode' => $sub['plan']['plan_code'])),
          t('<h6 style="padding:0;margin:0">@unit_price</h6>', array('@unit_price' => recurly_format_price($sub['pending_subscription']['unit_amount_in_cents'], $sub['currency']))),
          t('@state', array('@state' => 'pending')),
          t('@setup_fee', array('@setup_fee' => 'N/A')),
          t('@trial_start', array('@trial_start' => 'N/A')),
          t('@current_start', array('@current_start' => 'N/A')),
          t('<h6 style="padding:0;margin:0;color:#FF0000">@current_end</h6>', array('@current_end' => 'will start on ' . recurly_format_date($sub['current_period_ends_at']))),
        );
      }
    }

    if (empty($rows)) {
      $rows[] = array(array('data' => t('No subscription plans found.'), 'colspan' => 7));
    }

    $build['recurly_subscription_table'] = array(
      '#theme' => 'table', 
      '#header' => $header, 
      '#rows' => $rows, 
      '#attributes' => array('id' => 'recurly-subscription'), 
      '#empty' => t('No subscriptions available.'),
    );

    // return $build;
    $output .= render($build);

    $title = t('Subscription Details for @code', array('@code' => $account_code));
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_render($title, $output);
    // above command will exit().
  }
  else {
    drupal_set_title(t('Subscription Details'));
    return drupal_set_message(t('No account code selected'), 'warning');
  }
}

/* callback returns transaction data from Recurly in a in a table view for an account code */
function recurly_transactions_view($account_code = FALSE) {
    
  if ($account_code) { 
    $output = '';
    // Initialize the Recurly client with the site-wide settings.
    if (!recurly_client_initialize()) {
      $output = t('Could not initialize the Recurly client.');
    }

    $transactions = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts/' . $account_code . '/transactions?per_page=' . RECURLY_ACCOUNT_PERPAGE, '', TRUE);

    // Format the invoice data into a table for display.
    $header = array( t('Created'), t('Amount'), t('Action'), t('Status'), t('Recurring'), t('Card Details'));

    foreach ($transactions as $trans) {
      $bill = $trans['details']['account']['billing_info'];
      $rows[] = array(
        t('@created', array('@created' => recurly_format_date($trans['created_at']))),
        t('@amount', array('@amount' => (isset($trans['amount_in_cents'])) ? recurly_format_price($trans['amount_in_cents'], $trans['currency']) : '')),
        t('@action', array('@action' => $trans['action'])),
        t('@status', array('@status' => $trans['status'])),
        t('@rec', array('@rec' => $trans['recurring'])),
        t('<span id="card-type">@card</span> x @m/@y ending in @last4', array('@card' => $bill['card_type'], '@m' => $bill['month'], '@y' => $bill['year'], '@last4' => $bill['last_four'])),
      );
    }

    if (empty($rows)) {
      $rows[] = array(array('data' => t('No transactions found.'), 'colspan' => 6));
    }

    $build['recurly_transactions_table'] = array(
      '#theme' => 'table', 
      '#header' => $header, 
      '#rows' => $rows, 
      '#attributes' => array('id' => 'recurly-transactions'), 
      '#empty' => t('No transactions available.'),
    );

    // return $build;
    $output .= render($build);
      
    $title = t('Transactions Details for @code', array('@code' => $account_code));
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_render($title, $output);
    // above command will exit().
  }
  else {
    drupal_set_title(t('Transactions Details'));
    return drupal_set_message(t('No account code selected'), 'warning');
  }
}

/* callback returns invoice data from Recurly in a in a table view for an account code */
function recurly_invoices_view($account_code=FALSE) {
    
  if ($account_code) { 
    $output = '';
    // Initialize the Recurly client with the site-wide settings.
    if (!recurly_client_initialize()) {
      $output = t('Could not initialize the Recurly client.');
    }

    $invoices = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts/' . $account_code . '/invoices?per_page=' . RECURLY_ACCOUNT_PERPAGE, '', TRUE);

    // Format the invoice data into a table for display.
    $header = array(t('Invoice'), t('Description'), t('Period'), t('State'), t('Amount'),  t('Created'), t('Origin'));

    foreach ($invoices as $invoice) {
      $adj = $invoice['line_items']['adjustment'];
      $rows[] = array(
        t('@num', array('@num' => $invoice['invoice_number'])),
        t('@desc', array('@desc' => $adj['description'])),
        t('@current_start @current_end', array('@current_start' => recurly_format_date($adj['start_date']), '@current_end' => (!is_array($adj['end_date'])) ? ' - ' . recurly_format_date($adj['end_date']) : '')),
        t('@state', array('@state' => $invoice['state'])),
        t('@amount', array('@amount' => (isset($invoice['total_in_cents'])) ? recurly_format_price($adj['total_in_cents'], $adj['currency']) : '')),
        t('@created', array('@created' => recurly_format_date($invoice['created_at']))),
        t('@origin', array('@origin' => $adj['origin'])),
      );
    }

    if (empty($rows)) {
      $rows[] = array(array('data' => t('No invoices found.'), 'colspan' => 7));
    }

    $build['recurly_invoice_table'] = array(
      '#theme' => 'table', 
      '#header' => $header, 
      '#rows' => $rows, 
      '#attributes' => array('id' => 'recurly-invoice'), 
      '#empty' => t('No invoices available.'),
    );

      // return $build;
    $output .= render($build);
      
    $title = t('Invoice Details for @code', array('@code' => $account_code));
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_render($title, $output);
    // above command will exit().
  }
  else {
    drupal_set_title(t('Subscription Details'));
    return drupal_set_message(t('No account code selected'), 'warning');
  }
}

/* returns all active accounts with all subscriptions attached to them, and saves each of them (merges) to the local database */
function recurly_accounts_overview($reset_cache = FALSE) {

  if (!$reset_cache) {
    $data = cache_get('recurly-accounts-all:' . variable_get('recurly_subdomain', ''));

    // If accounts were found, return them now.
    if (!empty($data->data)) {
      drupal_set_message(t('pulling Recurly data from cache as of @date', array('@date' => format_date($data->created, 'long'))));
      recurly_add_js();
      return $data->data;
    }
  }
     
  // Initialize the Recurly client with the site-wide settings.
  if (!recurly_client_initialize()) {
    return t('Could not initialize the Recurly client.');
  }

  // Retrieve all active accounts.
  $active_accounts = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts?state=active&per_page=' . RECURLY_ACCOUNT_PERPAGE, '', TRUE);

  // Retrieve all past_due accounts.
  $past_due_accounts = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts?state=past_due&per_page=' . RECURLY_ACCOUNT_PERPAGE, '', TRUE);

  // Note we are not going to pull in any closed accounts here (that's the only other state an account can be in)
  $accounts = array_merge($active_accounts, $past_due_accounts);
  
  // bail if no accounts found
  if (empty($accounts)) {
    return drupal_set_message(t('No active or past_due accounts found'), 'warning');  
  }
  
  // Retrieve all subscriptions just as above
  $subs = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'subscriptions?per_page=' . RECURLY_ACCOUNT_PERPAGE, '', TRUE);

  // Format the data into a table for display.
  $header = array(t('Account Code'), t('Username'), t('Name'), t('State'), t('Email'), t('Created'), t('Subscription'), t('Actions'));

  $rows = array();
  foreach ($accounts as &$account) {

    // get the uid for this account from recurly_account table (will be 0 if not found)
    $uid = recurly_get_uid($account['account_code']);

    // if we can't find a Drupal user, try by email
    if ($uid && isset($account['email']) && !empty($account['email']) && $acct_by_mail = user_load_by_mail($account['email'])) {
      $uid = $acct_by_mail->uid;
    }

    // get all subscriptions for this account
    $account['subs'] = array();
    $plancodes = array();
    foreach ($subs as $subscription => $sub) {
      if (str_replace('https://api.recurly.com/v2/accounts/', '', $sub['account']['@attributes']['href']) == $account['account_code']) {
        $account['subs'][] = $sub;
        $plancodes[] = $sub['plan']['plan_code'];
        // now remove this from
      }
    }
    
    if (count($plancodes) === 1) {
      $plan_code_display = $plancodes[0];
    }
    else {
      $plan_code_display = (!empty($plancodes)) ? theme('item_list', array('items' => $plancodes, 'type' => 'ol')) : 'no plan';      
    }
    
    // At this point we have an account with all subscriptions attached to it

    $operations = array();
    
    // Add subscription, invoices, transactions and local links
    $operations[] = array(
      'title' => t('Subscription'),
      'attributes' => array('class' => 'subscriptions ctools-use-modal ctools-modal-ctools-licenses-style', 'rel' => 'subscription', 'title' => 'subscription'),
      'href' => 'admin/config/services/recurly/subscription/' . $account['account_code'],
    );
    $operations[] = array(
      'title' => t('Invoices'),
      'attributes' => array('class' => 'invoices ctools-use-modal ctools-modal-ctools-licenses-style', 'rel' => 'invoices', 'title' => 'invoices'),
      'href' => 'admin/config/services/recurly/invoices/' . $account['account_code'],
    );
    $operations[] = array(
      'title' => t('Transactions'),
      'attributes' => array('class' => 'transactions ctools-use-modal ctools-modal-ctools-licenses-style', 'rel' => 'transactions', 'title' => 'transactions'),
      'href' => 'admin/config/services/recurly/transactions/' . $account['account_code'],
    );

    if ($uid > 0) {
      $operations[] = array(
        'title' => t('User'),
        'attributes' => array('class' => 'local no-ajax', 'rel' => 'local', 'title' => 'local user'),
        'href' => 'user/' . $uid,
      );        
    }
    else {
      $operations[] = array(
        'title' => t('User'),
        'attributes' => array('class' => 'local unfound no-ajax', 'rel' => 'local', 'title' => 'local user not found'),
        'href' => '',
      );
    }
        
    $rows[] = array(
      t('@code', array('@code' => $account['account_code'])),
      t('@username', array('@username' => (is_string($account['username'])) ? $account['username'] : 'unspecified')),
      t('@first @last', array('@first' => (is_string($account['first_name'])) ? $account['first_name'] : '',
        '@last' => (is_string($account['last_name'])) ? $account['last_name'] : '')),
      t('@state', array('@state' => (is_string($account['state'])) ? $account['state'] : 'unspecified')),
      t('@email', array('@email' => (is_string($account['email'])) ? $account['email'] : 'unspecified')),
      t('@created', array('@created' => recurly_format_date($account['created_at']))),
      $plan_code_display,
      theme('links', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline')))),
    );
    
    if ($uid) {
      recurly_account_save($account, $uid, $account); // note here we are saving the entire account as 'data' (with subscription(s) attached)!
    }

    if (module_exists('commerce_recurly_setup') && $setup = variable_get('recurly_import_setup', FALSE)) {
      // if setup module is installed for commerce_recurly now we save the account with uid of 0 to get them into the system in the first place
      if ($uid == 0) recurly_account_save($account, $uid, $account);
      // if setup module is installed and create local subscriptions form is submitted, then import the subscribers
      if ($create = variable_get('recurly_import_create_subscribers', FALSE) && count($plancodes) >= 1) {
        for ($i=0; $i<count($plancodes); $i++) {
          if ($uid > 0 || $import_anonymous = variable_get('recurly_import_anonymous', FALSE)) {
            commerce_recurly_setup_create_local_subscription($plancodes[$i], $uid, $account['account_code'], $account['subs'][$i]);         
          }
        }
      }
    }    
  }

  recurly_add_js();
  $build['recurly_accounts_table'] = array(
    '#theme' => 'table', 
    '#header' => $header, 
    '#rows' => $rows, 
    '#attributes' => array('id' => 'recurly-accounts'), 
    '#empty' => t('No accounts available.'),
  );

  // If data was actually returned, cache it for the current subdomain.
  if (!empty($build)) {
    cache_set('recurly-accounts-all:' . variable_get('recurly_subdomain', ''), $build, 'cache', CACHE_TEMPORARY);
  }
  return $build;      
}

/* Returns a uid for an $account_code from recurly_account table or 0 if not found */
function recurly_get_uid($account_code) {
  $sql = "SELECT uid FROM {recurly_account} WHERE account_code = :code";
  $args = array(':code' => $account_code);
  $data = db_query($sql, $args)->fetchField();
  // if nothing found, it could be that there is no local record yet so return empty
  return (!empty($data)) ? $data : 0;
}

/* Fetches account data from Recurly for one account code via curl. Tries to find subscriptions as well and attaches them as ['subs'] */
function recurly_get_account_data($account_code) {
  $recurly_acc = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts/' . $account_code, '', FALSE, TRUE);
  $recurly_subs = recurly_send_request('GET', RECURLY_V2_ENDPOINT . 'accounts/' . $account_code . '/subscriptions');

  // if no account found, return FALSE
  if (!$recurly_acc) {
    return FALSE;
    // watchdog the error?
  }

  $recurly_acc['subs'] = array();

  foreach ($recurly_subs->body['subscription'] as $sub) {
    $recurly_acc['subs'][] = $sub;
  }
  return $recurly_acc;
}

/**
 * Taken from the Recurly 2.0 API, to parse the links in the header of a cURL request 
 */
function recurly_get_curl_links($header) {
  $links = array();
  preg_match_all('/\<([^>]+)\>; rel=\"([^"]+)\"/', $header, $matches);
  if (sizeof($matches) > 2) {
    for ($i = 0; $i < sizeof($matches[1]); $i++) {
      $links[$matches[2][$i]] = $matches[1][$i];
    }
  }
  return $links;
}

/**
 * Taken from the Recurly 2.0 API, this simply takes a method, a url and option xml and returns an array of elements: statusCode, 
 * header, body(array of data converted from the xml string) and a main_element which is the main element of the xml string returned.
 * If $all is TRUE, then the function will iterate through pages of results to retreive them all and return one numerically keyed array.
 * If $single is TRUE, then the function will return the body (useful when you are expecting just one result i.e. an account)
 */
function recurly_send_request($method, $url, $xml = '', $all = FALSE, $single = FALSE) {
  // this is the new approach taken from client.php in the API
  $client = new Recurly_Client();
  $request = $client->request($method, $url, $xml);
  $object = simplexml_load_string($request->body);
  $request->body = @json_decode(@json_encode($object), 1);
  $request->main_element = $object->getName();

  // check statusCode is in range and if not there are errors, so put them on the object before continuing?
  
  // if just a single record was requested return the body now
  if ($single) {
    return $request->body;
  }
  // type will be the singular version of the main_element in most cases but just in case...
  $type = ($request->main_element[strlen($request->main_element)-1] == 's') ? substr($request->main_element, 0, -1) : $request->main_element;

  // if there is only one record, we need to move results down a level in the array
  if (!isset($request->body[$type][0]) && isset($request->body[$type]['uuid'])) {
    $replace = $request->body[$type];
    unset($request->body[$type]);
    $request->body[$type][] = $replace;
  }
  
  // if there are no results at all, then $request->body[$type] will not be set, so set it
  if (!isset($request->body[$type])) {
    $request->body[$type] = array();
  }
  // if (!isset($request->body[$type][0]) || empty($request->body[$type][0])) {
  //   return FALSE;
  // }
  if (isset($request->headers['Link']) && !empty($request->headers['Link'])) {
    $request->links = recurly_get_curl_links($request->headers['Link']);
  }
  // if all the data is to be collected then continue to fetch results until there are no more
  if ($all) {
    // init our data array
    $data = $request->body[$type];
    $next = isset($request->links['next']) ? $request->links['next'] : FALSE;

    while ($next != FALSE) {
      $request = $client->request($method, $next, $xml, $type);
      $object = simplexml_load_string($request->body);
      $request->body = @json_decode(@json_encode($object),1);
      if (isset($request->headers['Link']) && !empty($request->headers['Link'])) {
        $request->links = recurly_get_curl_links($request->headers['Link']);
      }
      // if there is only one record, we need to move results down a level in the array
      if (!isset($request->body[$type][0]) && isset($request->body[$type]['uuid'])) {
        $replace = $request->body[$type];
        unset($request->body[$type]);
        $request->body[$type][] = $replace;
      }
      if (isset($request->body[$type][0]) && !empty($request->body[$type][0])) {
        $data = array_merge($request->body[$type], $data);
      }
      $next = isset($request->links['next']) ? $request->links['next'] : FALSE;
    }
    return $data; 
  }
  else {
    return $request;      
  }
}
